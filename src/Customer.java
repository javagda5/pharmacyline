public class Customer implements Comparable<Customer> {
    private int age;
    private boolean isPregnant;
    private String name;

    private long timestampAdded;

    public Customer(int age, boolean isPregnant, String name) {
        this.age = age;
        this.isPregnant = isPregnant;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isPregnant() {
        return isPregnant;
    }

    public void setPregnant(boolean pregnant) {
        isPregnant = pregnant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTimestampAdded() {
        return timestampAdded;
    }

    public void setTimestampAdded(long timestampAdded) {
        this.timestampAdded = timestampAdded;
    }

    @Override
    public int compareTo(Customer other) {
        // zakladam ze -1 to gora
//        if (isPregnant && other.isPregnant) {
//            return 0;
//        } else if (isPregnant && !other.isPregnant) {
//            return -1;
//        } else if (!isPregnant && other.isPregnant) {
//            return 1;
//        }
//        if (age > 60 && other.age > 60) {
//            return 0;
//        } else if (age > 60 && other.age < 60) {
//            return -1;
//        } else if (age < 60 && other.age > 60) {
//            return 1;
//        }
//
//        return 0;
        if (other.isPregnant) return 1; // 1 w dol
        if (this.isPregnant) return -1; // -1 gora
        if (this.age == other.age) {
            return 0;
        } else {
            if (other.age > 60) return 1;
            if (this.age > 60) return -1;
        }
        if(timestampAdded == other.timestampAdded) return 0;
        if (other.timestampAdded > timestampAdded) return -1;
        if (timestampAdded > other.timestampAdded) return 1;
        return 0;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "age=" + age +
                ", isPregnant=" + isPregnant +
                ", name='" + name + '\'' +
                '}';
    }
}

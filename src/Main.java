public class Main {
    public static void main(String[] args) {
        Pharmacy p = new Pharmacy();

        p.addCustomer(new Customer(20, true, "sara"));
        p.addCustomer(new Customer(20, false, "klara"));
        p.addCustomer(new Customer(50, false, "lara"));

        p.print();

        p.addCustomer(new Customer(61, false, "wara"));
        p.print();
        p.addCustomer(new Customer(18, true, "nagle"));
        p.print();
        p.addCustomer(new Customer(40, false, "ziomek"));
        p.print();
        p.addCustomer(new Customer(199, false, "ziomek2"));
        p.print();
    }
}

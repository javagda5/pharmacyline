import java.sql.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Pharmacy {
    private PriorityQueue<Customer> queue;

    public Pharmacy() {
        queue = new PriorityQueue<>();
    }

    public void addCustomer(Customer c){
        c.setTimestampAdded(System.nanoTime());
        queue.add(c);
    }

    public void print(){
        Customer[] array = new Customer[queue.size()];
        array = queue.toArray(array);
        Arrays.sort(array);
        for (Customer c : array) {
            System.out.println(c);
        }
        System.out.println();
    }

    public void printEmpty(){
        while(!queue.isEmpty()){
            Customer c = queue.poll();
            System.out.println(c);
        }
        System.out.println();
    }

    class CustomerComparator implements Comparator<Customer> {
        @Override
        public int compare(Customer customer, Customer t1) {
            return customer.compareTo(t1);
        }
    }
}
